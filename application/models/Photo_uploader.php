<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Photo_uploader extends CI_Model {

    #--------------------------------
    #       end function org_upload;
    #--------------------------------
    function do_upload($FILES, $sizes, $uid) {
        $max_file_size = 1024 * 1024; // 1MB
        $valid_exts = array('jpeg', 'jpg', 'gif', 'png', 'webp');
        $directory_thumb = array('storage/thumb', 'storage/medium', 'storage/master');
        if ($FILES['size'] < $max_file_size) {
            // get file extension
            $ext = strtolower(pathinfo($FILES['name'], PATHINFO_EXTENSION));
            if (in_array($ext, $valid_exts)) {
                /* resize image */
                $k = 0;
                // $uid = uniqid();
                foreach ($sizes as $w => $h) {
                    $files[] = $this->resize($w, $h, $FILES, $directory_thumb[$k], $uid);
                    $k++;
                }
            } else {
                $files['msg'] = $msg = 'Archivo no soportado';
            }
        } else {
            $files['msg'] = $msg = 'Sube una imagen de menos de 1 MB';
        }
        return $files;
    }

    #-------------------------------------------------
    # end function pb_delete_temp;
    function resize($width, $height, $FILES, $directory, $uid) {
        $sssss = $FILES['size'] / 1024;
        list($w, $h) = getimagesize($FILES['tmp_name']);
        $ratio = max($width / $w, $height / $h);
        $h = ceil($height / $ratio);
        $x = ($w - $width / $ratio) / 2;
        $w = ceil($width / $ratio);

        $ext = explode(".", $FILES['name']);
        $string = str_replace(' ', '-', $ext[0]);

        $path = $directory . '/' . $string . '_'. $uid . '.' . end($ext);

        if ($sssss < 100000 && ($directory == 'uploads')) {
            copy($FILES['tmp_name'], $path);
        }

        /* read binary data from image file */ 
        $imgString = file_get_contents($FILES['tmp_name']);
        /* create image from string */
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);
        imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);

        /* Save image */
        switch ($FILES['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 70);
                break;
            case 'image/png':
                imagepng($tmp, $path, 7);
                break;
            case 'image/gif':
                imagegif($tmp, $path, 0);
                break;
            case 'image/webp':
                imagewebp($tmp, $path, 70);
                break;
            default:
                exit;
                break;
        }
        return $path;
        /* cleanup memory */
        imagedestroy($image);
        imagedestroy($tmp);
    }

}
