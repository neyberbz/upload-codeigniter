<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	// constructor function
    public function __construct() {
        parent::__construct();
        $this->load->model('photo_uploader', 'photo');
    }

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function image_upload() {

        $this->load->library('image_lib');
        $sizes = array(120 => 84, 648 => 374, 960 => 552);

        $pst_imge = $_FILES['image']['name'];
        $image_chk = explode(".",$pst_imge);
        $extent = end($image_chk);

        if($extent=="jpg" || $extent=="jpeg" || $extent=="gif" || $extent=="png" || $extent=="webp"){
			$uid = uniqid();
            $file_location = $this->photo->do_upload($_FILES['image'], $sizes, $uid);
            $name = $image_chk[0] . '_' . $uid . '.' . $extent;
			$data = array(
				'code'		=> 200,
				'message'	=> "$name se ha súbido al servidor completamente.",
				'photo'		=> $name,
				'uid'		=> $uid,
			);
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
        } else{
			$data = array(
				'code'		=> 500,
				'message'	=> "move_uploaded_file la función falló.",
			);
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
            exit;
        }
    }

	function remove_upload() {
		$this->load->helper("file");
		$name = $this->input->post('name');
		// print_r($name); die();
        $link = 'storage/thumb/'.$name;
        $linkme = 'storage/medium/'.$name;
        $linkma = 'storage/master/'.$name;
        unlink($link);
        unlink($linkme);
        unlink($linkma);
		echo "eliminado";
    }

}
