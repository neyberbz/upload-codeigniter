<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Welcome to CodeIgniter</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	</head>
<body>

<div class="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div class="row" style="padding: 20px;">
		<form id="upload_form" enctype="multipart/form-data" method="post">
			<input type="hidden" name="uid" id="uid" value="<?=uniqid();?>">
			<div class="custom-file">
				<input type="file" name="image" id="image" class="custom-file-input">
				<label class="custom-file-label" for="customFile">Seleccionar archivo JPG, PNG...</label>
			</div>
			<button class="btn btn-success" type="button" onclick="uploadFile()">Subir archivo</button>
			<div class="progress my-2" style="height: 10px;">
				<div id="progressBar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
			
		</form>
	</div>
	<div class="row bg-light rounded text-center py-1">
		<h6 id="status" class="d-block w-100 mb-0"></h6>
	</div>
	<div class="row border rounded p-2" id="content_galery"></div>
	<div class="row bg-warning rounded text-center py-1">
		<p id="loaded_n_total" class="d-block w-100 small text-muted mb-0"></p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script>
	/* Video Tutorial: http://www.youtube.com/watch?v=EraNFJiY0Eg */
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
	function _(el){
		return document.getElementById(el);
	}
	function uploadFile(){
		// alert(file.name+" | "+file.size+" | "+file.type);
		let file = _("image").files[0],
			uid = document.querySelector("#uid").value;
			name = file.name,
			formdata = new FormData(),
			formdata.append("image", file),
			formdata.append("uid", uid);
		// alert(Math.round(file.size/1024/1024));
		// if(Math.round(file.size/1024/1024) >= 1) 
		let ajax = new XMLHttpRequest();
		ajax.responseType = 'json';
		ajax.upload.addEventListener("progress", progressHandler, false);
		ajax.addEventListener("load", completeHandler, false);
		ajax.addEventListener("error", errorHandler, false);
		ajax.addEventListener("abort", abortHandler, false);
		ajax.open("POST", "http://localhost:8080/upload-codeigniter/welcome/image_upload", true);
		ajax.send(formdata);
	}
	function progressHandler(event){
		_("loaded_n_total").innerHTML = "Subiendo "+event.loaded+" bytes de "+event.total;
		var percent = (event.loaded / event.total) * 100;
		_("progressBar").style.width = Math.round(percent) + '%';
    	_("progressBar").setAttribute('aria-valuenow', Math.round(percent));
		_("status").innerHTML = Math.round(percent)+"% subiendo... espere por favor";
	}
	function completeHandler(event){
		_("status").innerHTML = event.target.response.message;
		_("progressBar").style.width = '0%';
		_("progressBar").setAttribute('aria-valuenow', 0);
		$('.custom-file-input').siblings(".custom-file-label").addClass("selected").html("Seleccionar archivo JPG, PNG...");
		imgRender(event.target.response.photo, event.target.response.photo, event.target.response.uid);
		document.querySelector('#upload_form').reset()
	}
	function errorHandler(event){
		_("status").innerHTML = "Subida de archivo fallida";
	}
	function abortHandler(event){
		_("status").innerHTML = "Subida de archivo cancelado";
	}
	function imgRender (img, name, uid) {
		// añade el elemento creado y su contenido al DOM
		let contentGalery = document.querySelector("#content_galery"),
			add = `
			<div class="mb-3 w-100 col-4 item" id="item_${uid}">
				<button type="button" class="close" style="position: absolute; top: 15px; right: 30px; z-index: 99;" onclick="removeImg('item_${uid}', '${name}')">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="row no-gutters bg-light rounded p-2">
					<div class="col-md-12">
						<figure class="figure mb-1">
							<img src="http://localhost:8080/upload-codeigniter/storage/medium/${img}" class="figure-img img-fluid rounded mb-0">
							<figcaption class="figure-caption bg-light"><small class="form-text text-white bg-secondary rounded p-1 mt-0">${name}</small></figcaption>
						</figure>
					</div>
					<div class="col-md-12">
						<div class="form-group mb-1">
							<small class="form-text text-muted">Nombre de la Imagen*</small>
							<input class="form-control form-control-sm" name="imgTitulo[]" type="text" placeholder="Título">
							<input class="form-control form-control-sm" name="imgName[]" type="hidden" value="${name}">
						</div>
						<div class="form-group mb-1">
							<small class="form-text text-muted">Descripción de la Imagen*</small>
							<textarea class="form-control form-control-sm" name="imgDescripcion[]" rows="2" placeholder="Descripción"></textarea>
						</div>
					</div>
				</div>
			</div>`;
			contentGalery.insertAdjacentHTML("beforeend" ,add);
	}
	function removeImg(uid, name){
		// console.log(name);
		if (window.confirm(`Desea eliminar la imagen ${name}?`)) {
			const data = new FormData();
			data.append('name', name);
			fetch('http://localhost:8080/upload-codeigniter/welcome/remove_upload', {
				method: 'POST',
				body: data
			})
			.then(function(response) {
				if(response.ok) {
					return response.text()
				} else {
					throw "Error en la llamada Ajax";
				}
			})
			.then(function(texto) {
				// console.log(texto);
				let img = document.querySelector(`#${uid}`);
				img.remove();
				alert("Imagen eliminada correctamente!")
			})
			.catch(function(err) {
				console.log(err);
			});
			// let img = document.querySelector(`#${uid}`);
			// img.remove();
		}
	}
	</script>
</body>
</html>
